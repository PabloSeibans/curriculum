import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { ContenidoComponent } from './components/contenido/contenido.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { APP_ROUTING } from './app.routes';


import { FormsModule , } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

// Import the module from the SDK
import { TareasComponent } from './components/tareas/tareas.component';
import { TareaFormComponent } from './components/tarea-form/tarea-form.component';
import { TareaListaComponent } from './components/tarea-lista/tarea-lista.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    InicioComponent,
    ContenidoComponent,
    ContactoComponent,
    TareasComponent,
    TareaFormComponent,
    TareaListaComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatProgressSpinnerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
