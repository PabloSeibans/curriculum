import { Component, OnInit } from '@angular/core';
import { TareaService } from "../../services/tarea.service";
import { Tarea } from '../../models/task';

@Component({
  selector: 'app-tarea-lista',
  templateUrl: './tarea-lista.component.html',
  styleUrls: ['./tarea-lista.component.css']
})
export class TareaListaComponent implements OnInit {
  tareas: Tarea[];

  constructor(public tareaServicio: TareaService) { }

  ngOnInit(): void {
    this.tareas = this.tareaServicio.getTasks();
  }

  addTask(tarea: Tarea) {
    this.tareaServicio.addTarea(tarea);
  }

}